﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("AutoClicker")]
[assembly: AssemblyDescription("Automatic clicking via hotkey")]
[assembly: AssemblyCompany("Syroot")]
[assembly: AssemblyProduct("Syroot.AutoClicker")]
[assembly: AssemblyCopyright("(c) Syroot, licensed under MIT")]
[assembly: AssemblyVersion("1.0.0")]
[assembly: AssemblyFileVersion("1.0.0")]
[assembly: ComVisible(false)]
[assembly: Guid("c7c4668f-8099-487b-acd5-2dbad8ffa73e")]
