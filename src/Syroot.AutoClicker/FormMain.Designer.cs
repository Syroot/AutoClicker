﻿namespace Syroot.AutoClicker
{
    partial class FormMain
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this._niMain = new System.Windows.Forms.NotifyIcon(this.components);
            this._timerClick = new System.Windows.Forms.Timer(this.components);
            this._cmMain = new System.Windows.Forms.ContextMenu();
            this._miSettings = new System.Windows.Forms.MenuItem();
            this._miSeparator = new System.Windows.Forms.MenuItem();
            this._miExit = new System.Windows.Forms.MenuItem();
            this._gbGeneral = new System.Windows.Forms.GroupBox();
            this._nmRepetition = new System.Windows.Forms.NumericUpDown();
            this._lbRepetition = new System.Windows.Forms.Label();
            this._lbHotkey = new System.Windows.Forms.Label();
            this._cbHotkey = new System.Windows.Forms.ComboBox();
            this._gbGeneral.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._nmRepetition)).BeginInit();
            this.SuspendLayout();
            // 
            // _niMain
            // 
            this._niMain.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this._niMain.BalloonTipTitle = "Welcome to AutoClicker!";
            this._niMain.Icon = ((System.Drawing.Icon)(resources.GetObject("_niMain.Icon")));
            this._niMain.Text = "AutoClicker";
            this._niMain.Visible = true;
            this._niMain.DoubleClick += new System.EventHandler(this._niMain_DoubleClick);
            // 
            // _timerClick
            // 
            this._timerClick.Interval = 1;
            this._timerClick.Tick += new System.EventHandler(this._timerClick_Tick);
            // 
            // _cmMain
            // 
            this._cmMain.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this._miSettings,
            this._miSeparator,
            this._miExit});
            // 
            // _miSettings
            // 
            this._miSettings.DefaultItem = true;
            this._miSettings.Index = 0;
            this._miSettings.Text = "Settings...";
            this._miSettings.Click += new System.EventHandler(this._miSettings_Click);
            // 
            // _miSeparator
            // 
            this._miSeparator.Index = 1;
            this._miSeparator.Text = "-";
            // 
            // _miExit
            // 
            this._miExit.Index = 2;
            this._miExit.Text = "Exit";
            this._miExit.Click += new System.EventHandler(this._miExit_Click);
            // 
            // _gbGeneral
            // 
            this._gbGeneral.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._gbGeneral.Controls.Add(this._nmRepetition);
            this._gbGeneral.Controls.Add(this._lbRepetition);
            this._gbGeneral.Controls.Add(this._lbHotkey);
            this._gbGeneral.Controls.Add(this._cbHotkey);
            this._gbGeneral.Location = new System.Drawing.Point(12, 12);
            this._gbGeneral.Name = "_gbGeneral";
            this._gbGeneral.Size = new System.Drawing.Size(310, 81);
            this._gbGeneral.TabIndex = 0;
            this._gbGeneral.TabStop = false;
            this._gbGeneral.Text = "General Settings";
            // 
            // _nmRepetition
            // 
            this._nmRepetition.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._nmRepetition.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this._nmRepetition.Location = new System.Drawing.Point(135, 51);
            this._nmRepetition.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this._nmRepetition.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this._nmRepetition.Name = "_nmRepetition";
            this._nmRepetition.Size = new System.Drawing.Size(169, 23);
            this._nmRepetition.TabIndex = 4;
            this._nmRepetition.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this._nmRepetition.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this._nmRepetition.ValueChanged += new System.EventHandler(this._nmRepetition_ValueChanged);
            // 
            // _lbRepetition
            // 
            this._lbRepetition.AutoSize = true;
            this._lbRepetition.Location = new System.Drawing.Point(6, 53);
            this._lbRepetition.Name = "_lbRepetition";
            this._lbRepetition.Size = new System.Drawing.Size(123, 15);
            this._lbRepetition.TabIndex = 3;
            this._lbRepetition.Text = "Repetition Speed (ms)";
            // 
            // _lbHotkey
            // 
            this._lbHotkey.AutoSize = true;
            this._lbHotkey.Location = new System.Drawing.Point(6, 25);
            this._lbHotkey.Name = "_lbHotkey";
            this._lbHotkey.Size = new System.Drawing.Size(95, 15);
            this._lbHotkey.TabIndex = 3;
            this._lbHotkey.Text = "Toggling Hotkey";
            // 
            // _cbHotkey
            // 
            this._cbHotkey.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._cbHotkey.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._cbHotkey.FormattingEnabled = true;
            this._cbHotkey.Location = new System.Drawing.Point(135, 22);
            this._cbHotkey.Name = "_cbHotkey";
            this._cbHotkey.Size = new System.Drawing.Size(169, 23);
            this._cbHotkey.TabIndex = 2;
            this._cbHotkey.SelectedValueChanged += new System.EventHandler(this._cbHotkey_SelectedValueChanged);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(334, 104);
            this.Controls.Add(this._gbGeneral);
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "AutoClicker Options";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormMain_FormClosing);
            this._gbGeneral.ResumeLayout(false);
            this._gbGeneral.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._nmRepetition)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.NotifyIcon _niMain;
        private System.Windows.Forms.Timer _timerClick;
        private System.Windows.Forms.ContextMenu _cmMain;
        private System.Windows.Forms.MenuItem _miExit;
        private System.Windows.Forms.GroupBox _gbGeneral;
        private System.Windows.Forms.NumericUpDown _nmRepetition;
        private System.Windows.Forms.Label _lbRepetition;
        private System.Windows.Forms.Label _lbHotkey;
        private System.Windows.Forms.ComboBox _cbHotkey;
        private System.Windows.Forms.MenuItem _miSettings;
        private System.Windows.Forms.MenuItem _miSeparator;
    }
}

