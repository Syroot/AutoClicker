﻿using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using static Syroot.AutoClicker.WinApi.User32;

namespace Syroot.AutoClicker
{
    /// <summary>
    /// Represents the main window of the application.
    /// </summary>
    public partial class FormMain : Form
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private bool _allowShow;
        private bool _allowExit;
        private Keys _hotkey;
        private int _hotkeyID = 1;

        // ---- CONSTRUCTORS -------------------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="FormMain"/> class.
        /// </summary>
        public FormMain()
        {
            InitializeComponent();
            foreach (Keys key in Enum.GetValues(typeof(Keys)).Cast<Keys>())
                if (key != Keys.None)
                    _cbHotkey.Items.Add(key);
            _niMain.ContextMenu = _cmMain;

            // Load settings.
            // TODO: Load settings from file instead.
            Hotkey = Keys.NumPad3;
            _cbHotkey.SelectedItem = Keys.NumPad3;
            _nmRepetition.Value = 100;

            _niMain.ShowBalloonTip(3000);
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        private Keys Hotkey
        {
            get => _hotkey;
            set
            {
                if (_hotkey != value)
                {
                    // Unregister an existing hotkey first.
                    if (_hotkey != Keys.None && !UnregisterHotKey(Handle, _hotkeyID))
                        throw new InvalidOperationException("Could not unregister hotkey.");
                    _hotkey = value;
                    // Register the new hotkey.
                    if (_hotkey != Keys.None && !RegisterHotKey(Handle, _hotkeyID, 0, (uint)_hotkey))
                        throw new InvalidOperationException("Could not register hotkey.");
                    _niMain.BalloonTipText = "You can toggle clicking by pressing the " + _hotkey + " key.";
                }
            }
        }

        // ---- METHODS (PROTECTED) ------------------------------------------------------------------------------------

        /// <summary>
        /// Controls if the specified visibility can be set.
        /// </summary>
        /// <param name="value">The visibility state.</param>
        protected override void SetVisibleCore(bool value)
        {
            base.SetVisibleCore(_allowShow ? value : _allowShow);
        }

        /// <summary>
        /// The windows message procedure.
        /// </summary>
        /// <param name="m">The windows message.</param>
        protected override void WndProc(ref Message m)
        {
            base.WndProc(ref m);

            // Check if we got a hotkey pressed.
            if ((WindowMessage)m.Msg == WindowMessage.Hotkey)
                _timerClick.Enabled = !_timerClick.Enabled;
        }

        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        private void ShowNearTray()
        {
            _allowShow = true;
            if (!Visible)
            {
                Rectangle workingArea = Screen.PrimaryScreen.WorkingArea;
                Location = new Point(workingArea.Width - Width, workingArea.Height - Height);
                Show();
            }
        }

        // ---- EVENTHANDLERS ------------------------------------------------------------------------------------------

        private void FormMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (_allowExit)
            {
                _niMain.Visible = false;
            }
            else
            {
                e.Cancel = true;
                Hide();
            }
        }

        private void _cbHotkey_SelectedValueChanged(object sender, EventArgs e)
        {
            Hotkey = (Keys)_cbHotkey.SelectedItem;
        }

        private void _nmRepetition_ValueChanged(object sender, EventArgs e)
        {
            _timerClick.Interval = (int)_nmRepetition.Value;
        }

        private void _niMain_DoubleClick(object sender, EventArgs e)
        {
            ShowNearTray();
        }

        private void _miSettings_Click(object sender, EventArgs e)
        {
            ShowNearTray();
        }

        private void _miExit_Click(object sender, EventArgs e)
        {
            _allowExit = true;
            Close();
        }

        private void _timerClick_Tick(object sender, EventArgs e)
        {
            mouse_event(MouseEventFlags.LeftDown | MouseEventFlags.LeftUp,
                (uint)Cursor.Position.X, (uint)Cursor.Position.Y, 0, 0);
        }
    }
}
