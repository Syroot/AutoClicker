﻿using System;
using System.Runtime.InteropServices;

namespace Syroot.AutoClicker.WinApi
{
    internal static class User32
    {
        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        [DllImport(nameof(User32))]
        internal static extern void mouse_event(MouseEventFlags dwFlags, uint dx, uint dy, uint dwData,
            uint dwExtraInfo);

        [DllImport(nameof(User32))]
        internal static extern bool RegisterHotKey(IntPtr hWnd, int id, uint fsModifiers, uint vk);

        [DllImport(nameof(User32))]
        internal static extern bool UnregisterHotKey(IntPtr hWnd, int id);

        // ---- CLASSES, STRUCTS & ENUMS -------------------------------------------------------------------------------

        internal enum WindowMessage : uint
        {
            Hotkey = 0x312
        }

        [Flags]
        internal enum MouseEventFlags : uint
        {
            LeftDown = 1 << 1,
            LeftUp = 1 << 2,
            RightDown = 1 << 3,
            RightUp = 1 << 4
        }
    }
}
