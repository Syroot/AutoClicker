# AutoClicker

This is a small Windows Forms which sits in the tray and waits for you to press a specific key which you can configure.
When it is hit, it toggles spamming mouse clicks in a specified delay at the current cursor position.

It internally uses the user32::mouse_event procedure to generate clicks.
